use twmap::{Image, LoadMultiple, Sound, TwMap};

use clap::Parser;
use sha2::{Digest, Sha256};

use std::error::Error;
use std::fs;
use std::io::Write;
use std::path::PathBuf;

#[derive(Parser, Debug)]
#[clap(author, version)]
#[clap(
    about = "Extracts images and sounds from Teeworlds/DDNet maps. Saves them to the current working directory"
)]
struct Cli {
    #[clap(required = true, value_parser)]
    maps: Vec<PathBuf>,
    /// Only extract image files (.png)
    #[clap(long, value_parser, group = "specify-files")]
    images: bool,
    /// Only extract sound files (.opus)
    #[clap(long, value_parser, group = "specify-files")]
    sounds: bool,
    /// Omit sha256 postfix -> allow collisions in file names
    #[clap(long, value_parser)]
    omit_hashes: bool,
}

fn main() {
    let cli: Cli = Cli::parse();

    for file_path in &cli.maps {
        let map = match TwMap::parse_file(file_path) {
            Ok(map) => map,
            Err(err) => {
                eprintln!("{:?}: {}", file_path, err);
                continue;
            }
        };
        println!("Map: {:?}", file_path);
        if !cli.sounds {
            match save_images(map.images, cli.omit_hashes) {
                Ok(_) => {}
                Err(error) => eprintln!("{:?}: {}", file_path, error),
            }
        }
        if !cli.images {
            match save_sounds(map.sounds, cli.omit_hashes) {
                Ok(_) => {}
                Err(error) => eprintln!("{:?}: {}", file_path, error),
            }
        }
    }
}

fn save_images(mut images: Vec<Image>, omit_hashes: bool) -> Result<(), Box<dyn Error>> {
    images.load()?;

    for image in images {
        if let Image::Embedded(image) = image {
            let path = match omit_hashes {
                false => format!(
                    "{}_{}.png",
                    &image.name,
                    sha256(image.image.unwrap_ref().as_ref())
                ),
                true => format!("{}.png", &image.name),
            };
            image.image.unwrap().save(&path)?;
            println!("\t{}", &path);
        }
    }
    Ok(())
}

fn save_sounds(mut sounds: Vec<Sound>, omit_hashes: bool) -> Result<(), Box<dyn Error>> {
    sounds.load()?;

    for sound in sounds {
        let data = sound.data.unwrap();
        let path = match omit_hashes {
            false => format!("{}_{}.opus", &sound.name, sha256(&data)),
            true => format!("{}.opus", &sound.name),
        };
        let mut file = fs::File::create(&path)?;
        file.write_all(&data[..])?;
        println!("\t{}", &path);
    }

    Ok(())
}

fn sha256(data: &[u8]) -> String {
    let mut hasher = Sha256::new();
    hasher.update(data);
    let result = hasher.finalize();
    format!("{:x}", result)
}
