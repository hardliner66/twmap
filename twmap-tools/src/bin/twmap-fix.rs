use twmap::datafile::RawDatafile;
use twmap::{Layer, TwMap};

use clap::Parser;

use std::fs;
use std::path::PathBuf;
use std::process::exit;
use twmap::edit::ZeroUnusedParts;

#[derive(Parser, Debug)]
#[clap(author, version)]
#[clap(about = "Applies specific changes to Teeworlds/DDNet maps that can fix some broken ones")]
struct Cli {
    #[clap(value_parser)]
    input_map: PathBuf,
    #[clap(value_parser)]
    output_map: PathBuf,
    /// Goes through all quads and removes faulty ones
    #[clap(long, value_parser)]
    remove_faulty_quads: bool,
    /// Only extract sound files (.opus)
    #[clap(long, value_parser, group = "specify-files")]
    sounds: bool,
    /// Omit sha256 postfix -> allow collisions in file names
    #[clap(long, value_parser)]
    omit_hashes: bool,
    /// Zero the parts of tiles that are unused and can't be edited with the common tools
    #[clap(long, value_parser)]
    zero_unused_tile_parts: bool,
    /// Remove all envelope items before parsing the map from the datafile
    #[clap(long, value_parser)]
    remove_all_envelopes: bool,
}

fn main() {
    let cli: Cli = Cli::parse();

    let data = match fs::read(&cli.input_map) {
        Ok(data) => data,
        Err(err) => {
            println!("io error: {}", err);
            exit(1);
        }
    };

    let mut datafile = match RawDatafile::parse(&data) {
        Ok(df) => df.to_datafile(),
        Err(err) => {
            println!("datafile parsing error: {}", err);
            exit(1);
        }
    };

    if cli.remove_all_envelopes {
        // overwrite envelope items with an empty vector
        if let Some(removed_envelopes) = datafile.items.remove(&3) {
            println!(
                "removed {} envelopes (and their points)",
                removed_envelopes.len()
            );
        } else {
            println!("there were no envelopes to remove, envelope points will still be removed");
        }
        // overwrite envelope points with an empty vector
        if let Some(env_point_items) = datafile.items.get_mut(&6) {
            if let Some(env_point_item) = env_point_items.get_mut(0) {
                env_point_item.item_data = Vec::new();
            }
        }
    }

    let mut map = match TwMap::parse_datafile_unchecked(&datafile) {
        Err(err) => {
            println!("load error: {}", err);
            exit(1);
        }
        Ok(map) => map,
    };

    if let Err(err) = map.load_unchecked() {
        println!("error on decompression: {}", err);
        exit(1);
    }

    if cli.remove_faulty_quads {
        remove_faulty_quads(&mut map);
    }

    if cli.zero_unused_tile_parts {
        let map_before = map.clone();
        map.edit_tiles::<ZeroUnusedParts>();
        if map != map_before {
            println!("zeroed tile parts!");
        } else {
            println!("there were no tile parts to zero out!");
        }
    }

    // (try to) save map
    if let Err(err) = map.save_file(&cli.output_map) {
        println!("failed to save map: {}", err);
        exit(1);
    }
}

fn remove_faulty_quads(map: &mut TwMap) {
    let mut count = 0;

    for group in &mut map.groups {
        for layer in &mut group.layers {
            if let Layer::Quads(quad_layer) = layer {
                let mut to_remove = Vec::new();

                for (i, quad) in quad_layer.quads.iter().enumerate() {
                    if let Err(err) = quad.check(&map.envelopes) {
                        to_remove.push(i);
                        count += 1;
                        println!(
                            "\t{:?}\n\t\tThe {}th quad will be removed.\n\t\tQuad's position: {}",
                            err,
                            i + 1,
                            quad.position
                        );
                    }
                }

                for &i in to_remove.iter().rev() {
                    quad_layer.quads.remove(i);
                }
            }
        }
    }
    println!("Removed {} quad(s) in total.", count);
}
