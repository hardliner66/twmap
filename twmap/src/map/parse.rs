use crate::compression;
use crate::compression::{compress, decompress, ZlibDecompressionError};
use crate::convert::{To, TryTo};
use crate::datafile::{Datafile, DatafileParseError, Item, RawDatafile};
use crate::map::*;

use bitflags::bitflags;
use fixed::types::{I17F15, I22F10, I27F5};
use log::{info, warn};
use structview::{i32_le, View};
use thiserror::Error;

use fixed::traits::Fixed;
use std::cmp::Ordering;
use std::collections::{HashMap, HashSet};
use std::convert::TryInto;
use std::fmt;
use std::fs;
use std::path::Path;

#[derive(Error, Debug)]
#[error("{0}")]
pub enum MapFromDatafileError {
    Datafile(#[from] DatafileParseError),
    UuidIndex(#[from] UuidIndexError),
    MapVersion(#[from] MapVersionError),
    Info(#[from] InfoError),
    Image(#[from] ImageError),
    Envelope(#[from] EnvelopeError),
    EnvPoint(#[from] EnvPointError),
    Group(#[from] GroupError),
    Layer(#[from] LayerError),
    Sound(#[from] SoundError),
    AutoMapper(#[from] AutoMapperError),
}

impl TwMap {
    /// For parsing a binary map file.
    pub fn parse_file<P: AsRef<Path>>(path: P) -> Result<TwMap, Error> {
        let map = TwMap::parse_file_unchecked(path)?;
        map.check()?;
        Ok(map)
    }

    pub fn parse_file_unchecked<P: AsRef<Path>>(path: P) -> Result<TwMap, Error> {
        let data = fs::read(path)?;
        let map = TwMap::parse_unchecked(&data)?;
        Ok(map)
    }

    /// For parsing binary map data.
    pub fn parse(data: &[u8]) -> Result<TwMap, Error> {
        let map = TwMap::parse_unchecked(data)?;
        map.check()?;
        Ok(map)
    }

    pub fn parse_unchecked(data: &[u8]) -> Result<TwMap, MapFromDatafileError> {
        let raw_datafile = RawDatafile::parse(data)?;
        let datafile = raw_datafile.to_datafile();

        TwMap::parse_datafile_unchecked(&datafile)
    }

    /// For parsing the datafile of a binary map.
    pub fn parse_datafile(df: &Datafile) -> Result<TwMap, Error> {
        let map = TwMap::parse_datafile_unchecked(df)?;
        map.check()?;
        Ok(map)
    }

    pub fn parse_datafile_unchecked(df: &Datafile) -> Result<TwMap, MapFromDatafileError> {
        let uuid_index = df.uuid_index()?;

        MapVersion::parse(df)?;
        let info = Info::parse(df)?;
        let images = Image::parse_all(df)?;

        let (mut envelopes, version, env_point_amounts) = Envelope::parse_all(df)?;
        let total_point_amount = env_point_amounts.iter().map(|&n| n.to::<u64>()).sum();
        let envelope_points = EnvPoint::parse_all(df, version, total_point_amount)?;
        distribute_env_points(&mut envelopes, env_point_amounts, envelope_points);

        let (mut groups, layer_amounts) = Group::parse_all(df)?;
        let (layers, version) = Layer::parse_all(df)?;
        distribute_layers(&mut groups, layer_amounts, layers);
        let (auto_mappers, auto_mapper_positions) = AutomapperConfig::parse_all(df, &uuid_index)?;
        distribute_auto_mappers(&mut groups, auto_mappers, auto_mapper_positions)?;

        let sounds = Sound::parse_all(df)?;

        let mut map = TwMap {
            version,
            info,
            images,
            envelopes,
            groups,
            sounds,
        };
        let removed = map.remove_duplicate_physics_layers();
        if removed > 0 {
            warn!("Removed {} duplicate physics layers.", removed);
        }
        map.correct_physics_layer_name();
        Ok(map)
    }
}

// for item types types that should always be included and have only one item
#[derive(Error, Debug)]
pub enum MonoItemTypeError {
    #[error("Item type should only have one item")]
    MultipleItems,
}

impl Datafile<'_> {
    fn get_mono_item_type(&self, type_id: u16) -> Result<Option<&Item>, MonoItemTypeError> {
        if let Some(items) = self.items.get(&type_id) {
            if items.len() > 1 {
                Err(MonoItemTypeError::MultipleItems)
            } else {
                // the datafile parser guarantees at least one item of each included ItemType
                Ok(Some(&items[0]))
            }
        } else {
            Ok(None)
        }
    }
}

#[derive(Error, Debug)]
pub enum UuidIndexError {
    #[error("UUID index error: item is not of length 4")]
    ItemLength,
    #[error("UUID index error: An UUID was assigned twice to different type ids")]
    DuplicateUuid,
    #[error("UUID index error: An entry in the index is not an item type")]
    MissingItemType,
}

impl Datafile<'_> {
    // creates the mapping from uuids to type_ids
    fn uuid_index(&self) -> Result<HashMap<[u8; 16], u16>, UuidIndexError> {
        let uuid_items = match self.items.get(&0xffff) {
            None => return Ok(HashMap::new()),
            Some(uuid_items) => uuid_items,
        };
        let mut uuid_index = HashMap::new();
        for item in uuid_items {
            if item.item_data.len() != 4 {
                return Err(UuidIndexError::ItemLength);
            }
            let mut uuid = <[u8; 16]>::default();
            for (i, n) in item.item_data.iter().enumerate() {
                uuid[i * 4..(i + 1) * 4].copy_from_slice(&n.to_be_bytes())
            }

            if uuid_index.contains_key(&uuid) {
                if uuid_index[&uuid] == item.id {
                    info!("Ignored a duplicate entry in the UUID index");
                    continue;
                }
                return Err(UuidIndexError::DuplicateUuid);
            }
            if !self.items.contains_key(&item.id) {
                return Err(UuidIndexError::MissingItemType);
            }
            uuid_index.insert(uuid, item.id);
        }
        Ok(uuid_index)
    }
}

#[derive(Error, Debug)]
pub enum ItemError {
    #[error("the item data is too short (length: {}, required: {})", .0, .1)]
    TooShort(usize, usize),
    #[error("the item data is too long (length: {}, max: {})", .0, .1)]
    TooLong(usize, usize),
}

impl Item {
    fn min_length(&self, expected_length: usize) -> Result<(), ItemError> {
        if self.item_data.len() < expected_length {
            Err(ItemError::TooShort(self.item_data.len(), expected_length))
        } else {
            Ok(())
        }
    }

    fn max_length(&self, expected_length: usize) -> Result<(), ItemError> {
        if self.item_data.len() > expected_length {
            Err(ItemError::TooLong(self.item_data.len(), expected_length))
        } else {
            Ok(())
        }
    }
}

#[derive(Error, Debug)]
pub enum DataItemError {
    #[error("Decompression failed: {0}")]
    Decompression(compression::ZlibDecompressionError),
    #[error("The data index is negative")]
    NegativeIndex,
    #[error("The data index is out of bounds")]
    OutOfBounds,
}

impl Datafile<'_> {
    fn data_item(&self, index: i32) -> Result<(&[u8], usize), DataItemError> {
        let index = match index {
            i32::MIN..=-1 => return Err(DataItemError::NegativeIndex),
            0..=i32::MAX => index.try_to::<usize>(),
        };
        match self.data_items.get(index) {
            None => Err(DataItemError::OutOfBounds),
            Some((data, size)) => Ok((data.as_ref(), *size)),
        }
    }

    fn optional_data_item(&self, index: i32) -> Result<Option<(&[u8], usize)>, DataItemError> {
        match index {
            -1 => Ok(None),
            _ => Ok(Some(self.data_item(index)?)),
        }
    }

    fn decompressed_data_item(&self, index: i32) -> Result<Vec<u8>, DataItemError> {
        let (compressed_data, size) = self.data_item(index)?;
        let decompressed_data = match compression::decompress(compressed_data, size) {
            Err(err) => return Err(DataItemError::Decompression(err)),
            Ok(data) => data,
        };
        Ok(decompressed_data)
    }

    fn optional_decompressed_data_item(
        &self,
        index: i32,
    ) -> Result<Option<Vec<u8>>, DataItemError> {
        match index {
            -1 => Ok(None),
            _ => Ok(Some(self.decompressed_data_item(index)?)),
        }
    }
}

#[derive(Error, Debug)]
pub enum VersionError {
    #[error("Version is smaller than 1")]
    TooLow,
    #[error("Version is too high, thereby not supported")]
    Unsupported,
}

const fn parse_version(n: i32, min: i32, max: i32) -> Result<i32, VersionError> {
    if n < min {
        Err(VersionError::TooLow)
    } else if n > max {
        Err(VersionError::Unsupported)
    } else {
        Ok(n)
    }
}

#[derive(Error, Debug)]
pub enum MultipleVersionsError {
    #[error("{0}")]
    VersionNumber(#[from] VersionError),
    #[error("The version number is inconsistent with the previous items")]
    Inconsistent,
}

fn parse_compare_version(
    n: i32,
    min: i32,
    max: i32,
    expected_version: &mut Option<i32>,
) -> Result<i32, MultipleVersionsError> {
    let version = parse_version(n, min, max)?;
    match expected_version {
        None => {
            *expected_version = Some(version);
            Ok(version)
        }
        Some(expected_version) => {
            if version == *expected_version {
                Ok(version)
            } else {
                Err(MultipleVersionsError::Inconsistent)
            }
        }
    }
}

#[derive(Error, Debug)]
pub enum StringParseError {
    #[error("String was missing a nul byte")]
    NulByte,
}

fn parse_lossy_utf_string(data: &[u8], max_len: usize) -> String {
    let mut string = String::from_utf8_lossy(data).to_string();
    if string.len() > max_len {
        warn!("A string was encoded into invalid utf8 ('{}'). The invalid bytes were replaced with replacement characters, which resulted in a too long string. It will be cut off at the max size", string);
    }
    while string.len() > max_len {
        string.pop();
    }
    string
}

fn parse_c_string(data: &[u8], max_len: usize) -> Result<String, StringParseError> {
    // Remove nul byte first
    if data.last() != Some(&0) {
        return Err(StringParseError::NulByte);
    }
    Ok(parse_lossy_utf_string(&data[..data.len() - 1], max_len))
}

fn parse_i32_string(numbers: &[i32]) -> Result<String, StringParseError> {
    let mut string = Vec::new();
    for &n in numbers {
        string.extend_from_slice(&n.to_be_bytes());
    }

    if string.iter().all(|&c| c == 0) {
        info!("Completely zeroed out i32-string found, dropped");
        return Ok(String::new());
    }

    // Pop guaranteed nul byte
    if string.pop() != Some(0) {
        return Err(StringParseError::NulByte);
    }
    string = string.into_iter().map(|x| x.wrapping_add(128)).collect();
    // pop remaining nul bytes
    while string.last() == Some(&0) {
        string.pop();
    }
    Ok(parse_lossy_utf_string(&string, numbers.len() * 4 - 1))
}

#[derive(Error, Debug)]
pub struct BoolError;

impl fmt::Display for BoolError {
    fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
        write!(f, "Bool value is neither 0 nor 1")
    }
}

const fn parse_bool(value: i32) -> Result<bool, BoolError> {
    match value {
        0 => Ok(false),
        1 => Ok(true),
        _ => Err(BoolError),
    }
}

#[derive(Error, Debug)]
pub struct U8Error;

impl fmt::Display for U8Error {
    fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
        write!(f, "An u8 value is out of its range")
    }
}

fn parse_u8(value: i32) -> Result<u8, U8Error> {
    match value.try_into() {
        Ok(n) => Ok(n),
        Err(_) => Err(U8Error),
    }
}

#[derive(Error, Debug)]
pub struct U16OptionError;

impl fmt::Display for U16OptionError {
    fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
        write!(
            f,
            "An (internal) optional index was neither -1 or in its u16 range"
        )
    }
}

fn parse_u16_option(value: i32) -> Result<Option<u16>, U16OptionError> {
    if value == -1 {
        Ok(None)
    } else {
        match value.try_into() {
            Ok(n) => Ok(Some(n)),
            Err(_) => Err(U16OptionError),
        }
    }
}

#[derive(Error, Debug)]
#[error("Version error: {0}")]
pub enum MapVersionError {
    ItemType(#[from] MonoItemTypeError),
    Item(#[from] ItemError),
    Version(#[from] VersionError),
}

pub struct MapVersion {
    pub version: i32,
}

impl MapVersion {
    fn parse(df: &Datafile) -> Result<(), MapVersionError> {
        let version_item = match df.get_mono_item_type(0)? {
            Some(item) => item,
            None => return Ok(()),
        };
        version_item.min_length(1)?;
        version_item.max_length(1)?;
        parse_version(version_item.item_data[0], 1, 1)?;
        Ok(())
    }
}

#[derive(Error, Debug)]
#[error("Info error: {0}")]
pub enum InfoError {
    ItemType(#[from] MonoItemTypeError),
    Item(#[from] ItemError),
    Version(#[from] VersionError),
    DataItem(#[from] DataItemError),
    StringParse(#[from] StringParseError),
}

impl Info {
    fn parse(df: &Datafile) -> Result<Info, InfoError> {
        let info_item = match df.get_mono_item_type(1)? {
            Some(item) => item,
            None => return Ok(Info::default()),
        };

        info_item.min_length(5)?;
        let _version = parse_version(info_item.item_data[0], 1, 1)?;

        let max_lengths = [
            Info::MAX_AUTHOR_LENGTH,
            Info::MAX_VERSION_LENGTH,
            Info::MAX_CREDITS_LENGTH,
            Info::MAX_LICENSE_LENGTH,
        ];
        let mut strings = Vec::new();
        for (&data_index, &max_len) in info_item.item_data[1..5].iter().zip(max_lengths.iter()) {
            if let Some(data) = df.optional_decompressed_data_item(data_index)? {
                strings.push(parse_c_string(&data, max_len)?);
            } else {
                strings.push(String::new());
            }
        }

        let mut settings_strings = Vec::new();
        if info_item.item_data.len() > 5 {
            info_item.min_length(6)?;
            info_item.max_length(6)?;
            if let Some(mut data) = df.optional_decompressed_data_item(info_item.item_data[5])? {
                match data.pop() {
                    None => {}
                    Some(0) => {
                        for string_data in data.split(|&x| x == 0) {
                            let string = String::from_utf8_lossy(string_data).to_string();
                            settings_strings.push(string);
                        }
                    }
                    Some(_) => return Err(StringParseError::NulByte.into()),
                }
            }
        } else {
            info_item.max_length(5)?;
        }

        Ok(Info {
            author: strings.remove(0),
            version: strings.remove(0),
            credits: strings.remove(0),
            license: strings.remove(0),
            settings: settings_strings,
        })
    }
}

#[derive(Error, Debug)]
pub struct ImageError {
    kind: ImageErrorKind,
    index: usize,
}

impl fmt::Display for ImageError {
    fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
        write!(f, "Image Error at index {}: {}", self.index, self.kind)
    }
}

#[derive(Error, Debug)]
#[error("{0}")]
pub enum ImageErrorKind {
    Item(#[from] ItemError),
    Version(#[from] MultipleVersionsError),
    DataItem(#[from] DataItemError),
    StringParse(#[from] StringParseError),
    #[error("The external bool had an unexpected value")]
    ExternalBool,
    #[error("The image variant was set to another one than RGBA")]
    InvalidVariant,
    #[error("Negative width or height: {0}")]
    NegativeDimension(Point<i32>),
}

impl Image {
    fn parse_all(df: &Datafile) -> Result<Vec<Image>, ImageError> {
        if let Some(image_items) = df.items.get(&2) {
            let mut images = Vec::new();
            let mut version = None;
            for (index, image_item) in image_items.iter().enumerate() {
                images.push(
                    Image::parse(image_item, df, &mut version)
                        .map_err(|kind| ImageError { kind, index })?,
                );
            }
            Ok(images)
        } else {
            Ok(Vec::new())
        }
    }

    fn parse(
        item: &Item,
        df: &Datafile,
        version: &mut Option<i32>,
    ) -> Result<Image, ImageErrorKind> {
        item.min_length(6)?;
        let version = parse_compare_version(item.item_data[0], 1, 2, version)?;
        let size = Point::new(item.item_data[1], item.item_data[2]);
        let size = size
            .try_into()
            .ok_or(ImageErrorKind::NegativeDimension(size))?;

        let name_data = df.decompressed_data_item(item.item_data[4])?;
        let name = parse_c_string(&name_data, Image::MAX_NAME_LENGTH)?;

        let compressed_image =
            df.optional_data_item(item.item_data[5])?
                .map(|(data, data_size)| {
                    CompressedData::Compressed(data.to_vec(), data_size, ImageLoadInfo { size })
                });

        if item.item_data[3] != compressed_image.is_none().to::<i32>() {
            return Err(ImageErrorKind::ExternalBool);
        }

        if version > 1 {
            item.min_length(7)?;
            item.max_length(7)?;
            if item.item_data[6] != 1 {
                return Err(ImageErrorKind::InvalidVariant);
            }
        } else {
            item.max_length(6)?;
        }

        Ok(match compressed_image {
            Some(compressed_image) => EmbeddedImage {
                name,
                image: compressed_image,
            }
            .into(),
            None => ExternalImage { size, name }.into(),
        })
    }
}

#[derive(Error, Debug)]
pub struct EnvelopeError {
    kind: EnvelopeErrorKind,
    index: usize,
}

impl fmt::Display for EnvelopeError {
    fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
        write!(f, "Envelope error at index {}: {}", self.index, self.kind)
    }
}

#[derive(Error, Debug)]
#[error("{0}")]
pub enum EnvelopeErrorKind {
    Item(#[from] ItemError),
    Version(#[from] MultipleVersionsError),
    StringParse(#[from] StringParseError),
    #[error("The 'synchronized' field contains an invalid bool")]
    BoolParse(#[from] BoolError),
    #[error("Overlap in the envelope point ranges")]
    Overlap,
    #[error("Gap in the envelope point ranges")]
    Gap,
    #[error("The point amount is negative")]
    NegativeAmount,
    #[error("The type is invalid/unknown")]
    UnKnownType,
    #[error("In the first version of the envelope item, the name int always had to be -1")]
    FirstVersionName,
}

#[derive(Copy, Clone, Eq, PartialEq)]
pub enum EnvPointVersion {
    Normal,
    WithBezier,
}

impl Envelope {
    fn parse_all(
        df: &Datafile,
    ) -> Result<(Vec<Envelope>, EnvPointVersion, Vec<u32>), EnvelopeError> {
        if let Some(envelope_items) = df.items.get(&3) {
            let mut envelopes = Vec::new();
            let mut version = None;
            let mut point_amounts = Vec::new();
            let mut expected_start = 0;

            for (index, env_item) in envelope_items.iter().enumerate() {
                let (env, amount) = Envelope::parse(env_item, expected_start, &mut version)
                    .map_err(|kind| EnvelopeError { kind, index })?;
                envelopes.push(env);
                point_amounts.push(amount);
                expected_start += amount.to::<i64>();
            }
            let version = match version {
                Some(1) | Some(2) => EnvPointVersion::Normal,
                Some(3) => EnvPointVersion::WithBezier,
                _ => unreachable!(),
            };
            Ok((envelopes, version, point_amounts))
        } else {
            Ok((Vec::new(), EnvPointVersion::Normal, Vec::new()))
        }
    }

    fn parse(
        item: &Item,
        expected_start: i64,
        version: &mut Option<i32>,
    ) -> Result<(Envelope, u32), EnvelopeErrorKind> {
        item.min_length(5)?;
        let version = parse_compare_version(item.item_data[0], 1, 3, version)?;
        let start = item.item_data[2].to::<i64>();
        match start.cmp(&expected_start) {
            Ordering::Less => Err(EnvelopeErrorKind::Overlap),
            Ordering::Equal => Ok(()),
            Ordering::Greater => Err(EnvelopeErrorKind::Gap),
        }?;
        let amount = item.item_data[3];
        if amount < 0 {
            return Err(EnvelopeErrorKind::NegativeAmount);
        }

        let mut name = String::new();
        let mut synchronized = false;

        if item.item_data.len() > 5 {
            item.min_length(12)?;
            name = parse_i32_string(&item.item_data[4..12])?;
            if version >= 2 {
                item.min_length(13)?;
                item.max_length(13)?;
                synchronized = parse_bool(item.item_data[12])?;
            } else {
                item.max_length(12)?;
            }
        } else if item.item_data[4] != -1 {
            return Err(EnvelopeErrorKind::FirstVersionName);
        }
        Ok((
            match item.item_data[1] {
                1 => Envelope::Sound(Env {
                    name,
                    synchronized,
                    points: vec![],
                }),
                3 => Envelope::Position(Env {
                    name,
                    synchronized,
                    points: vec![],
                }),
                4 => Envelope::Color(Env {
                    name,
                    synchronized,
                    points: vec![],
                }),
                _ => return Err(EnvelopeErrorKind::UnKnownType),
            },
            amount.try_to(),
        ))
    }
}

fn parse_curve_type(id: i32, bezier: Option<&[i32]>) -> CurveKind<[i32; 4]> {
    match id {
        0 => CurveKind::Step,
        1 => CurveKind::Linear,
        2 => CurveKind::Slow,
        3 => CurveKind::Fast,
        4 => CurveKind::Smooth,
        5 => match bezier {
            None => CurveKind::Unknown(id),
            Some(data) => {
                debug_assert_eq!(data.len(), 16);
                CurveKind::Bezier(BezierCurve {
                    in_tangent_dx: data[0..4].try_into().unwrap(),
                    in_tangent_dy: data[4..8].try_into().unwrap(),
                    out_tangent_dx: data[8..12].try_into().unwrap(),
                    out_tangent_dy: data[12..16].try_into().unwrap(),
                })
            }
        },
        _ => CurveKind::Unknown(id),
    }
}

#[derive(Error, Debug)]
pub enum EnvPointError {
    #[error("Envelope point error: {0}")]
    ItemType(#[from] MonoItemTypeError),
    #[error("Envelope point error: Too little data")]
    TooLittleData,
    #[error("Envelope point error: Too much data")]
    TooMuchData,
}

// parse I32Color generically, since it uses all content-data bytes
// this allows all other point types to be converted from it
impl EnvPoint<[i32; 4]> {
    fn parse_all(
        df: &Datafile,
        version: EnvPointVersion,
        amount: u64,
    ) -> Result<Vec<EnvPoint<[i32; 4]>>, EnvPointError> {
        let env_point_item = match df.get_mono_item_type(6)? {
            Some(item) => item,
            None => return Ok(Vec::new()),
        };
        let env_point_data = &env_point_item.item_data;
        let size = match version {
            EnvPointVersion::Normal => 6,
            EnvPointVersion::WithBezier => 22,
        };
        match (amount * size).cmp(&(env_point_data.len().try_to())) {
            Ordering::Less => return Err(EnvPointError::TooLittleData),
            Ordering::Equal => {}
            Ordering::Greater => return Err(EnvPointError::TooMuchData),
        }
        let mut env_points = Vec::new();
        for i in 0..amount {
            let data = &env_point_data[(size * i).try_to()..(size * (i + 1)).try_to()];
            let env_point = EnvPoint::parse(data);
            env_points.push(env_point);
        }
        Ok(env_points)
    }

    fn parse(data: &[i32]) -> Self {
        match data.len() {
            6 => EnvPoint {
                time: data[0],
                content: data[2..6].try_into().unwrap(),
                curve: parse_curve_type(data[1], None),
            },
            22 => EnvPoint {
                time: data[0],
                content: data[2..6].try_into().unwrap(),
                curve: parse_curve_type(data[1], Some(&data[6..22])),
            },
            _ => unreachable!(),
        }
    }
}

impl From<[i32; 4]> for Position {
    fn from(vals: [i32; 4]) -> Self {
        Position {
            offset: Point::new(I17F15::from_bits(vals[0]), I17F15::from_bits(vals[1])),
            rotation: I22F10::from_bits(vals[2]),
        }
    }
}

impl From<[i32; 4]> for I32Color {
    fn from(vals: [i32; 4]) -> Self {
        I32Color {
            r: I22F10::from_bits(vals[0]),
            g: I22F10::from_bits(vals[1]),
            b: I22F10::from_bits(vals[2]),
            a: I22F10::from_bits(vals[3]),
        }
    }
}

impl From<[i32; 4]> for Volume {
    fn from(vals: [i32; 4]) -> Self {
        Volume(I22F10::from_bits(vals[0]))
    }
}

fn convert_curve_kind<T: From<[i32; 4]>>(curve: CurveKind<[i32; 4]>) -> CurveKind<T> {
    use CurveKind::*;
    match curve {
        Step => Step,
        Linear => Linear,
        Slow => Slow,
        Fast => Fast,
        Smooth => Smooth,
        Bezier(b) => Bezier(BezierCurve {
            in_tangent_dx: b.in_tangent_dx.into(),
            in_tangent_dy: b.in_tangent_dy.into(),
            out_tangent_dx: b.out_tangent_dx.into(),
            out_tangent_dy: b.out_tangent_dy.into(),
        }),
        Unknown(id) => Unknown(id),
    }
}

fn convert_env_points<T: From<[i32; 4]>>(points: &[EnvPoint<[i32; 4]>]) -> Vec<EnvPoint<T>> {
    points
        .iter()
        .map(|point| EnvPoint {
            time: point.time,
            content: point.content.into(),
            curve: convert_curve_kind(point.curve),
        })
        .collect()
}

fn distribute_env_points(
    envelopes: &mut [Envelope],
    amounts: Vec<u32>,
    points: Vec<EnvPoint<[i32; 4]>>,
) {
    let mut start = 0;
    for (env, amount) in envelopes.iter_mut().zip(amounts.into_iter()) {
        let env_points = &points[start..start + amount.try_to::<usize>()];
        start += amount.try_to::<usize>();
        match env {
            Envelope::Position(env) => env.points = convert_env_points(env_points),
            Envelope::Color(env) => env.points = convert_env_points(env_points),
            Envelope::Sound(env) => env.points = convert_env_points(env_points),
        }
    }
}

#[derive(Error, Debug)]
pub struct GroupError {
    kind: GroupErrorKind,
    index: Option<usize>,
}

impl fmt::Display for GroupError {
    fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
        write!(f, "Group error")?;
        if let Some(index) = self.index {
            write!(f, " at index {}", index)?;
        }
        write!(f, ": {}", self.kind)
    }
}

#[derive(Error, Debug)]
#[error("{0}")]
pub enum GroupErrorKind {
    Item(#[from] ItemError),
    Version(#[from] MultipleVersionsError),
    StringParse(#[from] StringParseError),
    #[error("The 'clipping' field contains an invalid bool")]
    BoolParse(#[from] BoolError),
    #[error("Overlap in the layer ranges")]
    Overlap,
    #[error("Gap in the layer ranges")]
    Gap,
    #[error("The layer amount is negative")]
    NegativeAmount,
}

impl Group {
    fn parse_all(df: &Datafile) -> Result<(Vec<Group>, Vec<u32>), GroupError> {
        if let Some(group_items) = df.items.get(&4) {
            let mut groups = Vec::new();
            let mut layer_amounts = Vec::new();
            let mut expected_start = 0;
            let mut version = None;

            for (i, item) in group_items.iter().enumerate() {
                let (group, amount) =
                    Group::parse(item, expected_start, &mut version).map_err(|kind| {
                        GroupError {
                            kind,
                            index: Some(i),
                        }
                    })?;
                groups.push(group);
                layer_amounts.push(amount);
                expected_start += amount.to::<i64>();
            }

            let total_layer_amount = match df.items.get(&5) {
                None => 0,
                Some(layer_items) => layer_items.len().try_to::<i64>(),
            };
            match expected_start.cmp(&total_layer_amount) {
                Ordering::Less => {
                    return Err(GroupError {
                        kind: GroupErrorKind::Gap,
                        index: None,
                    })
                }
                Ordering::Equal => {}
                Ordering::Greater => {
                    return Err(GroupError {
                        kind: GroupErrorKind::Overlap,
                        index: None,
                    })
                }
            }

            Ok((groups, layer_amounts))
        } else {
            Ok((Vec::new(), Vec::new()))
        }
    }

    fn parse(
        item: &Item,
        expected_start: i64,
        version: &mut Option<i32>,
    ) -> Result<(Group, u32), GroupErrorKind> {
        item.min_length(7)?;
        let version = parse_compare_version(item.item_data[0], 1, 3, version)?;

        let start = item.item_data[5].to::<i64>();
        match start.cmp(&expected_start) {
            Ordering::Less => Err(GroupErrorKind::Overlap),
            Ordering::Equal => Ok(()),
            Ordering::Greater => Err(GroupErrorKind::Gap),
        }?;
        let amount = item.item_data[6];
        if amount < 0 {
            return Err(GroupErrorKind::NegativeAmount);
        }
        let mut clipping = false;
        let mut clip_x = 0;
        let mut clip_y = 0;
        let mut clip_width = 0;
        let mut clip_height = 0;
        let mut name = String::new();
        if version >= 2 {
            item.min_length(12)?;
            clipping = parse_bool(item.item_data[7])?;
            clip_x = item.item_data[8];
            clip_y = item.item_data[9];
            clip_width = item.item_data[10];
            clip_height = item.item_data[11];

            if version >= 3 {
                item.min_length(15)?;
                item.max_length(15)?;
                name = parse_i32_string(&item.item_data[12..15])?;
            } else {
                item.max_length(12)?;
            }
        } else {
            item.max_length(7)?;
        }
        Ok((
            Group {
                name,
                offset: Point::new(
                    I27F5::from_bits(item.item_data[1]),
                    I27F5::from_bits(item.item_data[2]),
                ),
                parallax: Point::new(item.item_data[3], item.item_data[4]),
                layers: Vec::new(),
                clipping,
                clip: Point::new(I27F5::from_bits(clip_x), I27F5::from_bits(clip_y)),
                clip_size: Point::new(I27F5::from_bits(clip_width), I27F5::from_bits(clip_height)),
            },
            amount.try_to::<u32>(),
        ))
    }
}

impl TwMap {
    fn correct_physics_layer_name(&mut self) {
        if self
            .groups
            .iter()
            .filter(|group| group.is_physics_group())
            .count()
            == 1
        {
            let physics_group = self.physics_group_mut();
            if physics_group.name != "Game" {
                info!(
                    "Correcting the physics group name from '{}' to 'Game'",
                    physics_group.name
                );
                physics_group.name = "Game".into();
            }
        }
    }
}

#[derive(Error, Debug)]
pub struct LayerError {
    kind: LayerErrorKind,
    layer_kind: Option<LayerKind>,
    index: Option<usize>,
}

impl fmt::Display for LayerError {
    fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
        if let Some(kind) = self.layer_kind {
            write!(f, "{:?} layer error", kind)?;
        } else {
            write!(f, "Layer error")?;
        }
        if let Some(index) = self.index {
            write!(f, " at index {}", index)?;
        }
        write!(f, ": {}", self.kind)
    }
}

#[derive(Error, Debug)]
#[error("{0}")]
pub enum LayerErrorKind {
    Item(#[from] ItemError),
    DataItem(#[from] DataItemError),
    Version(#[from] MultipleVersionsError),
    StringParse(#[from] StringParseError),
    BoolParse(#[from] BoolError),
    U8Parse(#[from] U8Error),
    U16OptionParse(#[from] U16OptionError),
    #[error("Unknown layer flags were used")]
    LayerFlags,
    #[error("Layer was of an unknown type")]
    UnknownLayer,
    #[error("Layer was of an unknown tilemap layer type")]
    UnKnownTilemapLayer,
    #[error("Part of the layer dimensions was negative")]
    NegativeDimensions,
    #[error("The values for the unused parts  were not the default ones")]
    PhysicsLayerValues,
    #[error("The vanilla compatibility data is faulty")]
    CompatibilityData,
    #[error("The amount of objects is negative")]
    NegativeAmount,
    #[error("Too little data was provided for amount of objects")]
    TooLittleData,
    #[error("Too much data was provided for amount of objects")]
    TooMuchData,
    #[error("The shape of a sound source is invalid")]
    UnknownShape,
    #[error("No tilemap layer was found")]
    NoTilemapLayer,
    #[error("zlib decompression failed while trying to update outdated tiles - {0}")]
    OutdatedTilesCompression(ZlibDecompressionError),
    #[error("Updating resulted in a data size too big to fit into a i32: {0}")]
    OutdatedTilesDataSize(usize),
}

impl LayerKind {
    fn outdated_tile_versions(&self) -> &'static [OutdatedTileVersion] {
        use LayerKind::*;
        match self {
            Game => &[],
            Tiles => &[],
            Quads => unreachable!(),
            Front => &[],
            Tele => &[],
            Speedup => &SPEEDUP_VERSIONS[..],
            Switch => &SWITCH_VERSIONS[..],
            Tune => &[],
            Sounds => unreachable!(),
            Invalid(_) => unreachable!(),
        }
    }
}

fn convert_old_speedup(data: &[u8]) -> Vec<u8> {
    let mut speedup = [0; 6];
    speedup[0] = data[0];
    speedup[2] = 28;
    speedup[4] = data[2];
    speedup[5] = data[3];
    speedup.to_vec()
}

static SPEEDUP_VERSIONS: [OutdatedTileVersion; 1] = [OutdatedTileVersion {
    bytes_per_tile: 4,
    convert_fnc: convert_old_speedup,
}];

fn convert_tele_to_switch(data: &[u8]) -> Vec<u8> {
    let mut switch = [0; 4];
    switch[0] = data[0];
    switch[1] = data[1];
    switch.to_vec()
}

fn convert_old_switch(data: &[u8]) -> Vec<u8> {
    let mut switch = [0; 4];
    switch[0] = data[0];
    switch[1] = data[1];
    switch[2] = data[2];
    switch.to_vec()
}

static SWITCH_VERSIONS: [OutdatedTileVersion; 2] = [
    OutdatedTileVersion {
        bytes_per_tile: 2,
        convert_fnc: convert_tele_to_switch,
    },
    OutdatedTileVersion {
        bytes_per_tile: 3,
        convert_fnc: convert_old_switch,
    },
];

// tries to 'update' tile data from older tile versions
fn convert_to_new(data: &[u8], tile_version: &OutdatedTileVersion) -> Vec<u8> {
    let tiles = data
        .chunks(tile_version.bytes_per_tile)
        .map(|tile_data| (tile_version.convert_fnc)(tile_data));
    let mut altered_data = Vec::new();
    for mut tile in tiles {
        altered_data.append(&mut tile);
    }
    altered_data
}

fn try_correct_data(
    data: &[u8],
    data_size: usize,
    width: u32,
    height: u32,
    kind: LayerKind,
) -> Option<Result<(Vec<u8>, usize), LayerErrorKind>> {
    let outdated_versions = kind.outdated_tile_versions();
    for version in outdated_versions {
        let alt_size = width.try_to::<usize>() * height.try_to::<usize>() * version.bytes_per_tile;
        if data_size == alt_size {
            let decompressed_data = match decompress(data, data_size) {
                Err(err) => return Some(Err(LayerErrorKind::OutdatedTilesCompression(err))),
                Ok(data) => data,
            };
            let updated_data = convert_to_new(&decompressed_data, version);
            if updated_data.len() > i32::MAX.try_to::<usize>() {
                return Some(Err(LayerErrorKind::OutdatedTilesDataSize(
                    updated_data.len(),
                )));
            }
            return Some(Ok((compress(&updated_data), updated_data.len())));
        }
    }
    None
}

impl Layer {
    fn parse_all(df: &Datafile) -> Result<(Vec<Layer>, Version), LayerError> {
        if let Some(layer_items) = df.items.get(&5) {
            let mut layers = Vec::new();
            let mut versions = (None, None, None); // tilemap, quads, sound
            for (i, item) in layer_items.iter().enumerate() {
                let layer = Layer::parse(item, df, &mut versions).map_err(|kind| LayerError {
                    kind,
                    layer_kind: item.layer_kind().ok(),
                    index: Some(i),
                })?;
                layers.push(layer);
            }
            let map_version = match versions.0 {
                None => {
                    return Err(LayerError {
                        kind: LayerErrorKind::NoTilemapLayer,
                        layer_kind: None,
                        index: None,
                    })
                }
                Some(i32::MIN..=3) => Version::DDNet06,
                Some(4..=i32::MAX) => Version::Teeworlds07,
            };
            Ok((layers, map_version))
        } else {
            Err(LayerError {
                kind: LayerErrorKind::NoTilemapLayer,
                layer_kind: None,
                index: None,
            })
        }
    }

    fn parse(
        item: &Item,
        df: &Datafile,
        versions: &mut (Option<i32>, Option<i32>, Option<i32>),
    ) -> Result<Layer, LayerErrorKind> {
        use LayerKind::*;
        match item.layer_kind()? {
            Game | Tiles | Front | Tele | Speedup | Switch | Tune => {
                parse_tile_map_layer(item, df, &mut versions.0)
            }
            Quads => QuadsLayer::parse(item, df, &mut versions.1),
            Sounds => SoundsLayer::parse(item, df, &mut versions.2),
            Invalid(kind) => Ok(Layer::Invalid(kind)),
        }
    }
}

fn distribute_layers(groups: &mut [Group], amounts: Vec<u32>, mut layers: Vec<Layer>) {
    for (group, amount) in groups.iter_mut().zip(amounts.into_iter()) {
        for _ in 0..amount {
            group.layers.push(layers.remove(0))
        }
    }
}

impl LayerKind {
    pub(crate) fn data_index(&self) -> usize {
        debug_assert!(self.is_physics_layer() || *self == LayerKind::Tiles);
        use LayerKind::*;
        match self {
            Game | Tiles => 14,
            Front => 20,
            Tele => 18,
            Speedup => 19,
            Switch => 21,
            Tune => 22,
            _ => unreachable!(),
        }
    }

    pub(crate) fn static_name(&self) -> &'static str {
        debug_assert!(self.is_physics_layer() || *self == LayerKind::Tiles);
        use LayerKind::*;
        match self {
            Game => "Game",
            Front => "Front",
            Tele => "Tele",
            Speedup => "Speedup",
            Switch => "Switch",
            Tune => "Tune",
            _ => unreachable!(),
        }
    }
}

impl Item {
    fn layer_kind(&self) -> Result<LayerKind, LayerErrorKind> {
        use LayerKind::*;
        self.min_length(2)?;
        Ok(match self.item_data[1] {
            // so called 'LAYERTYPE'
            2 => {
                self.min_length(7)?;
                match self.item_data[6] {
                    // so called 'TILESLAYERFLAG'
                    0 => Tiles,
                    1 => Game,
                    2 => Tele,
                    4 => Speedup,
                    8 => Front,
                    16 => Switch,
                    32 => Tune,
                    _ => return Err(LayerErrorKind::UnKnownTilemapLayer),
                }
            }
            3 => Quads,
            9 | 10 => Sounds,
            _ => return Err(LayerErrorKind::UnknownLayer),
        })
    }
}

bitflags! {
    pub struct LayerFlags: i32 {
        const DETAIL = 0b1;
    }
}

fn parse_tile_map_layer(
    item: &Item,
    df: &Datafile,
    version: &mut Option<i32>,
) -> Result<Layer, LayerErrorKind> {
    use LayerKind::*;
    let kind = item.layer_kind()?;
    debug_assert!(kind.is_physics_layer() || kind == Tiles);
    item.min_length(15)?;
    let version = parse_compare_version(item.item_data[3], 1, 4, version)?;

    let flags = match LayerFlags::from_bits(item.item_data[2]) {
        Some(flags) => flags,
        None => return Err(LayerErrorKind::LayerFlags),
    };

    let size = Point::new(item.item_data[4], item.item_data[5])
        .try_into::<u32>()
        .ok_or(LayerErrorKind::NegativeDimensions)?;

    let color = Color {
        r: parse_u8(item.item_data[7])?,
        g: parse_u8(item.item_data[8])?,
        b: parse_u8(item.item_data[9])?,
        a: parse_u8(item.item_data[10])?,
    };
    let color_env = parse_u16_option(item.item_data[11])?;
    let color_env_offset = item.item_data[12];
    let image = parse_u16_option(item.item_data[13])?;

    let compression = match kind {
        Game | Tiles => version >= 4,
        _ => false,
    };
    let mut data_index = kind.data_index();
    let mut name = String::new();
    if version < 3 {
        // name was missing then
        if data_index > 14 {
            data_index -= 3;
        }
    } else {
        item.min_length(18)?;
        name = parse_i32_string(&item.item_data[15..18])?;
    }

    if kind.is_physics_layer() {
        if name != kind.static_name() {
            info!(
                "{:?} layer should have the name '{}', has name '{}' instead.",
                kind,
                kind.static_name(),
                name
            );
        }
        if color != Color::default() || color_env != None || color_env_offset != 0 || image != None
        {
            return Err(LayerErrorKind::PhysicsLayerValues);
        }
    }

    if kind != Game && kind != Tiles {
        let compatibility_tile_data =
            df.decompressed_data_item(item.item_data[Game.data_index()])?;
        if compatibility_tile_data.len().try_to::<u64>()
            != size.x.to::<u64>() * size.y.to::<u64>() * 4
        {
            return Err(LayerErrorKind::CompatibilityData);
        }
        if compatibility_tile_data.into_iter().any(|c| c != 0) {
            return Err(LayerErrorKind::CompatibilityData);
        }
    }

    item.min_length(data_index + 1)?;
    let (data, mut data_size) = df.data_item(item.item_data[data_index])?;
    let mut data = data.to_vec();

    if let Some(updated) = try_correct_data(&data, data_size, size.x, size.y, kind) {
        let (updated_data, updated_size) = updated?;
        data = updated_data;
        data_size = updated_size;
    }

    let load_info = TilesLoadInfo { size, compression };

    Ok(match kind {
        Tiles => Layer::Tiles(TilesLayer {
            name,
            detail: flags.contains(LayerFlags::DETAIL),
            color,
            color_env,
            color_env_offset,
            image,
            tiles: CompressedData::Compressed(data, data_size, load_info),
            automapper_config: AutomapperConfig::default(),
        }),
        Game => Layer::Game(GameLayer {
            tiles: CompressedData::Compressed(data, data_size, load_info),
        }),
        Front => Layer::Front(FrontLayer {
            tiles: CompressedData::Compressed(data, data_size, load_info),
        }),
        Tele => Layer::Tele(TeleLayer {
            tiles: CompressedData::Compressed(data, data_size, load_info),
        }),
        Speedup => Layer::Speedup(SpeedupLayer {
            tiles: CompressedData::Compressed(data, data_size, load_info),
        }),
        Switch => Layer::Switch(SwitchLayer {
            tiles: CompressedData::Compressed(data, data_size, load_info),
        }),
        Tune => Layer::Tune(TuneLayer {
            tiles: CompressedData::Compressed(data, data_size, load_info),
        }),
        _ => unreachable!(),
    })
}

#[repr(C)]
#[derive(View, Copy, Clone)]
struct BinaryColor {
    r: i32_le,
    g: i32_le,
    b: i32_le,
    a: i32_le,
}

impl BinaryColor {
    fn to_color(self) -> Result<Color, U8Error> {
        Ok(Color {
            r: parse_u8(self.r.to_int())?,
            g: parse_u8(self.g.to_int())?,
            b: parse_u8(self.b.to_int())?,
            a: parse_u8(self.a.to_int())?,
        })
    }
}

#[repr(C)]
#[derive(View, Copy, Clone)]
struct BinaryPoint {
    x: i32_le,
    y: i32_le,
}

impl BinaryPoint {
    fn to_point<T: Fixed<Bits = i32>>(self) -> Point<T> {
        Point {
            x: T::from_bits(self.x.to_int()),
            y: T::from_bits(self.y.to_int()),
        }
    }
}

#[repr(C)]
#[derive(View, Copy, Clone)]
struct BinaryQuad {
    corners: [BinaryPoint; 4],
    position: BinaryPoint,
    colors: [BinaryColor; 4],
    texture_coords: [BinaryPoint; 4],

    position_env: i32_le,
    position_env_offset: i32_le,
    color_env: i32_le,
    color_env_offset: i32_le,
}

impl BinaryQuad {
    fn to_quad(self) -> Result<Quad, LayerErrorKind> {
        let mut corners = <[Point<_>; 4]>::default();
        let mut colors = <[Color; 4]>::default();
        let mut texture_coords = <[Point<_>; 4]>::default();
        for i in 0..4 {
            corners[i] = self.corners[i].to_point();
            colors[i] = self.colors[i].to_color()?;
            texture_coords[i] = self.texture_coords[i].to_point();
        }
        Ok(Quad {
            corners,
            position: self.position.to_point(),
            colors,
            texture_coords,
            position_env: parse_u16_option(self.position_env.to_int())?,
            position_env_offset: self.position_env_offset.to_int(),
            color_env: parse_u16_option(self.color_env.to_int())?,
            color_env_offset: self.color_env_offset.to_int(),
        })
    }
}

impl QuadsLayer {
    fn parse(
        item: &Item,
        df: &Datafile,
        version: &mut Option<i32>,
    ) -> Result<Layer, LayerErrorKind> {
        debug_assert_eq!(item.layer_kind().unwrap(), LayerKind::Quads);
        item.min_length(7)?;
        let version = parse_compare_version(item.item_data[3], 1, 2, version)?;
        let flags = match LayerFlags::from_bits(item.item_data[2]) {
            Some(flags) => flags,
            None => return Err(LayerErrorKind::LayerFlags),
        };

        let quad_amount = item.item_data[4];
        if quad_amount < 0 {
            return Err(LayerErrorKind::NegativeAmount);
        }
        let quad_data = df.decompressed_data_item(item.item_data[5])?;
        match quad_data.len().cmp(&(quad_amount.try_to::<usize>() * 152)) {
            Ordering::Less => return Err(LayerErrorKind::TooLittleData),
            Ordering::Equal => {}
            Ordering::Greater => return Err(LayerErrorKind::TooMuchData),
        }
        let mut quads = Vec::new();
        for i in 0..quad_amount.try_to::<usize>() {
            let raw_quad = BinaryQuad::view(&quad_data[i * 152..(i + 1) * 152]).unwrap();
            quads.push(raw_quad.to_quad()?)
        }

        let image = parse_u16_option(item.item_data[6])?;
        let mut name = String::new();
        if version >= 2 {
            item.min_length(10)?;
            item.max_length(10)?;
            name = parse_i32_string(&item.item_data[7..10])?;
        } else {
            item.max_length(7)?;
        }

        Ok(Layer::Quads(QuadsLayer {
            name,
            detail: flags.contains(LayerFlags::DETAIL),
            quads,
            image,
        }))
    }
}

#[repr(C)]
#[derive(View, Copy, Clone)]
struct BinarySoundShape {
    kind: i32_le,
    value1: i32_le,
    value2: i32_le,
}

impl BinarySoundShape {
    fn to_shape(self) -> Result<SoundShape, LayerErrorKind> {
        match self.kind.to_int() {
            0 => Ok(SoundShape::Rectangle {
                size: Point::new(
                    I17F15::from_bits(self.value1.to_int()),
                    I17F15::from_bits(self.value2.to_int()),
                ),
            }),
            1 => Ok(SoundShape::Circle {
                radius: I27F5::from_bits(self.value1.to_int()),
            }),
            _ => Err(LayerErrorKind::UnknownShape),
        }
    }
}

#[repr(C)]
#[derive(View, Copy, Clone)]
struct BinarySoundSource {
    position: BinaryPoint,
    looping: i32_le,
    panning: i32_le,
    delay: i32_le,
    falloff: i32_le,
    position_env: i32_le,
    position_env_offset: i32_le,
    sound_env: i32_le,
    sound_env_offset: i32_le,
    shape: BinarySoundShape,
}

impl BinarySoundSource {
    fn to_source(self) -> Result<SoundSource, LayerErrorKind> {
        Ok(SoundSource {
            position: self.position.to_point(),
            looping: parse_bool(self.looping.to_int())?,
            panning: parse_bool(self.panning.to_int())?,
            delay: self.delay.to_int(),
            falloff: parse_u8(self.falloff.to_int())?,
            position_env: parse_u16_option(self.position_env.to_int())?,
            position_env_offset: self.position_env_offset.to_int(),
            sound_env: parse_u16_option(self.sound_env.to_int())?,
            sound_env_offset: self.sound_env_offset.to_int(),
            shape: self.shape.to_shape()?,
        })
    }
}

#[repr(C)]
#[derive(View, Copy, Clone)]
struct BinaryDeprecatedSoundSource {
    position: BinaryPoint,
    looping: i32_le,
    delay: i32_le,
    radius: i32_le,
    position_env: i32_le,
    position_env_offset: i32_le,
    sound_env: i32_le,
    sound_env_offset: i32_le,
}

impl BinaryDeprecatedSoundSource {
    fn to_source(self) -> Result<SoundSource, LayerErrorKind> {
        Ok(SoundSource {
            position: self.position.to_point(),
            looping: parse_bool(self.looping.to_int())?,
            panning: true,
            delay: self.delay.to_int(),
            falloff: 0,
            position_env: parse_u16_option(self.position_env.to_int())?,
            position_env_offset: self.position_env_offset.to_int(),
            sound_env: parse_u16_option(self.sound_env.to_int())?,
            sound_env_offset: self.sound_env_offset.to_int(),
            shape: SoundShape::Circle {
                radius: I27F5::from_bits(self.radius.to_int()),
            },
        })
    }
}

enum SoundsLayerVersion {
    Normal,
    Deprecated,
}

impl SoundsLayer {
    fn parse(
        item: &Item,
        df: &Datafile,
        version: &mut Option<i32>,
    ) -> Result<Layer, LayerErrorKind> {
        use SoundsLayerVersion::*;

        item.min_length(10)?;
        let _version = parse_compare_version(item.item_data[3], 2, 2, version)?;

        let sounds_layer_version = match item.item_data[1] {
            9 => Deprecated,
            10 => Normal,
            _ => unreachable!(),
        };
        let flags = match LayerFlags::from_bits(item.item_data[2]) {
            Some(flags) => flags,
            None => return Err(LayerErrorKind::LayerFlags),
        };

        let source_amount = item.item_data[4];
        if source_amount < 0 {
            return Err(LayerErrorKind::NegativeAmount);
        }
        let sound_source_data = df.decompressed_data_item(item.item_data[5])?;
        let source_len = match sounds_layer_version {
            Deprecated => 36,
            Normal => 52,
        };
        match sound_source_data
            .len()
            .cmp(&(source_amount.try_to::<usize>() * source_len))
        {
            Ordering::Less => return Err(LayerErrorKind::TooLittleData),
            Ordering::Equal => {}
            Ordering::Greater => return Err(LayerErrorKind::TooMuchData),
        }
        let mut sources = Vec::new();
        for i in 0..source_amount.try_to::<usize>() {
            let source_data = &sound_source_data[i * source_len..(i + 1) * source_len];
            let source = match sounds_layer_version {
                Deprecated => BinaryDeprecatedSoundSource::view(source_data)
                    .unwrap()
                    .to_source()?,
                Normal => BinarySoundSource::view(source_data).unwrap().to_source()?,
            };
            sources.push(source)
        }
        let sound = parse_u16_option(item.item_data[6])?;
        let name = parse_i32_string(&item.item_data[7..10])?;
        Ok(Layer::Sounds(SoundsLayer {
            name,
            detail: flags.contains(LayerFlags::DETAIL),
            sources,
            sound,
        }))
    }
}

#[derive(Error, Debug)]
pub struct SoundError {
    kind: SoundErrorKind,
    index: usize,
}

impl fmt::Display for SoundError {
    fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
        write!(f, "Sound Error at index {}: {}", self.index, self.kind)
    }
}

#[derive(Error, Debug)]
#[error("{0}")]
pub enum SoundErrorKind {
    Item(#[from] ItemError),
    Version(#[from] MultipleVersionsError),
    DataItem(#[from] DataItemError),
    StringParse(#[from] StringParseError),
    #[error("Can't be external")]
    ExternalBool,
    #[error("data_size and the actual data length don't match up")]
    DataSizeMismatch,
    #[error("data_size is negative")]
    NegativeDataSize,
}

impl Sound {
    fn parse_all(df: &Datafile) -> Result<Vec<Sound>, SoundError> {
        if let Some(sound_items) = df.items.get(&7) {
            let mut sounds = Vec::new();
            let mut version = None;
            for (index, sound_item) in sound_items.iter().enumerate() {
                sounds.push(
                    Sound::parse(sound_item, df, &mut version)
                        .map_err(|kind| SoundError { kind, index })?,
                );
            }
            Ok(sounds)
        } else {
            Ok(Vec::new())
        }
    }

    fn parse(
        item: &Item,
        df: &Datafile,
        version: &mut Option<i32>,
    ) -> Result<Sound, SoundErrorKind> {
        item.min_length(5)?;
        item.max_length(5)?;
        let _version = parse_compare_version(item.item_data[0], 1, 1, version)?;

        if item.item_data[1] != 0 {
            return Err(SoundErrorKind::ExternalBool);
        }

        let name_data = df.decompressed_data_item(item.item_data[2])?;
        let name = parse_c_string(&name_data, Sound::MAX_NAME_LENGTH)?;

        let (data, size) = df.data_item(item.item_data[3])?;
        let data = CompressedData::Compressed(data.to_vec(), size, ());

        if item.item_data[4] < 0 {
            return Err(SoundErrorKind::NegativeDataSize);
        }
        if item.item_data[4].try_to::<usize>() != size {
            return Err(SoundErrorKind::DataSizeMismatch);
        }

        Ok(Sound { name, data })
    }
}

#[derive(Error, Debug)]
pub struct AutoMapperError {
    kind: AutoMapperErrorKind,
    index: usize,
    position: Option<(i32, i32)>,
}

impl fmt::Display for AutoMapperError {
    fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
        write!(f, "Auto mapper error at index {}", self.index)?;
        if let Some((group, layer)) = self.position {
            write!(f, " (group {} -> layer {})", group, layer)?;
        }
        write!(f, ": {}", self.kind)
    }
}

#[derive(Error, Debug)]
#[error("{0}")]
pub enum AutoMapperErrorKind {
    Item(#[from] ItemError),
    U16OptionParse(#[from] U16OptionError),
    #[error("Unknown flags were used")]
    UnknownFlags,
    #[error("The group index is out of bounds")]
    GroupOob,
    #[error("The layer index is out of bounds")]
    LayerOob,
    #[error("Attached to an incompatible layer")]
    IncompatibleLayer,
    #[error("Another auto mapper is already attached to this layer")]
    SameLayer,
}

bitflags! {
    pub struct AutoMapperFlags: i32 {
        const AUTOMATIC = 0b1;
    }
}

pub type AutoMapperConfigs = (Vec<AutomapperConfig>, Vec<(i32, i32)>);

impl AutomapperConfig {
    pub const fn uuid() -> [u8; 16] {
        [
            0x3e, 0x1b, 0x27, 0x16, 0x17, 0x8c, 0x39, 0x78, 0x9b, 0xd9, 0xb1, 0x1a, 0xe0, 0x41,
            0xd, 0xd8,
        ]
    }

    fn parse_all(
        df: &Datafile,
        uuid_index: &HashMap<[u8; 16], u16>,
    ) -> Result<AutoMapperConfigs, AutoMapperError> {
        let uuid = AutomapperConfig::uuid();
        let type_id = match uuid_index.get(&uuid) {
            None => return Ok((Vec::new(), Vec::new())),
            Some(&type_id) => type_id,
        };
        let auto_mapper_items = &df.items[&type_id];

        let mut auto_mappers = Vec::new();
        let mut positions = Vec::new();
        for (index, item) in auto_mapper_items.iter().enumerate() {
            let (auto_mapper, pos) =
                AutomapperConfig::parse(item).map_err(|kind| AutoMapperError {
                    kind,
                    index,
                    position: AutomapperConfig::pos(item),
                })?;
            auto_mappers.push(auto_mapper);
            positions.push(pos);
        }
        Ok((auto_mappers, positions))
    }

    fn parse(item: &Item) -> Result<(AutomapperConfig, (i32, i32)), AutoMapperErrorKind> {
        item.min_length(6)?;
        item.max_length(6)?;
        let _version = item.item_data[0]; // was uninitialized
        let group = item.item_data[1];
        let layer = item.item_data[2];
        let config = parse_u16_option(item.item_data[3])?;
        let seed = item.item_data[4] as u32;
        let flags = match AutoMapperFlags::from_bits(item.item_data[5]) {
            Some(flags) => flags,
            None => return Err(AutoMapperErrorKind::UnknownFlags),
        };
        Ok((
            AutomapperConfig {
                config,
                seed,
                automatic: flags.contains(AutoMapperFlags::AUTOMATIC),
            },
            (group, layer),
        ))
    }

    fn pos(item: &Item) -> Option<(i32, i32)> {
        if item.item_data.len() < 3 {
            None
        } else {
            Some((item.item_data[1], item.item_data[2]))
        }
    }
}

fn attach_auto_mapper(
    groups: &mut [Group],
    position: (i32, i32),
    auto_mapper: AutomapperConfig,
    is_duplicate: bool,
) -> Result<(), AutoMapperErrorKind> {
    use Layer::*;
    if is_duplicate {
        return Err(AutoMapperErrorKind::SameLayer);
    }

    let (group_index, layer_index) = position;
    if group_index < 0 || group_index.try_to::<usize>() >= groups.len() {
        return Err(AutoMapperErrorKind::GroupOob);
    }
    let group = &mut groups[group_index.try_to::<usize>()];
    if layer_index < 0 || layer_index.try_to::<usize>() >= group.layers.len() {
        return Err(AutoMapperErrorKind::LayerOob);
    }
    let layer = &mut group.layers[layer_index.try_to::<usize>()];
    match layer {
        Tiles(l) => l.automapper_config = auto_mapper,
        Game(_) | Front(_) | Tele(_) | Speedup(_) | Switch(_) | Tune(_) | Invalid(_) => {}
        Quads(_) | Sounds(_) => return Err(AutoMapperErrorKind::IncompatibleLayer),
    }
    Ok(())
}

fn distribute_auto_mappers(
    groups: &mut [Group],
    auto_mappers: Vec<AutomapperConfig>,
    positions: Vec<(i32, i32)>,
) -> Result<(), AutoMapperError> {
    let mut attached_positions = HashSet::new();
    for (index, (auto_mapper, pos)) in auto_mappers
        .into_iter()
        .zip(positions.into_iter())
        .enumerate()
    {
        let is_duplicate = attached_positions.contains(&pos);
        attached_positions.insert(pos);
        attach_auto_mapper(groups, pos, auto_mapper, is_duplicate).map_err(|kind| {
            AutoMapperError {
                kind,
                index,
                position: Some(pos),
            }
        })?;
    }
    Ok(())
}
